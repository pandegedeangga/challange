<?php 
    $page = 'home'; 
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Timedoor Admin | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/bootstrap/bootstrap.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/font-awesome/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/Ionicons/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/admin.css') }}">
        <!-- TMDR Preset -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/tmdrPreset.css') }}">
        <!-- Custom css -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/custom.css') }}">
        <!-- Skin -->
        <link rel="stylesheet" href="{{ URL::asset('admin/css/skin.css') }}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/daterangepicker/daterangepicker.css') }}">
        <!-- DataTable -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/datatable/datatables.min.css') }}">
        <!-- DataTable -->
        <link rel="stylesheet" href="{{ URL::asset('admin/plugin/selectpicker/bootstrap-select.css') }}">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Header -->
                <!-- Logo -->
            <a href="{{ url('/home') }}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>T</b>D</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Timedoor</b> Admin</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">Hello, Admin </span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{ URL::asset('admin/img/user-ico.jpg') }}" class="img-circle" alt="User Image">
                        <p>
                            Administrator
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="text-right">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-flat">Sign out</button>
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                      <li class="<?php if ($page == 'home') { echo 'active'; } ?>">
                        <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> 
                          <span>Dashboard</span>
                        </a>
                      </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1.0
                </div>
                <strong>Copyright &copy; 2019 <a href="https://timedoor.net" class="text-green">Timedoor Indonesia</a>.</strong> All rights
                reserved.
            </footer>


            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <div class="text-center">
                                <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                            </div>
                        </div>
                        <div class="modal-body pad-20">
                            <p>Are you sure want to delete this item(s)?</p>
                        </div>
                        <div class="modal-footer">
                            <form name="form_delete" id="form_delete" action="" method="post">
                                @csrf
                                <input name="currentPage" type="hidden" value="">
                                <input name="id_delete" id="id_delete" type="hidden">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery 3 -->
        <script src="{{ URL::asset('admin/plugin/jquery/jquery.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::asset('admin/plugin/jquery/jquery-ui.min.js') }}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('admin/plugin/bootstrap/bootstrap.min.js') }}"></script>
        <!-- daterangepicker -->
        <script src="{{ URL::asset('admin/plugin/moment/moment.min.js') }}"></script>
        <script src="{{ URL::asset('admin/plugin/daterangepicker/daterangepicker.js') }}"></script>
        <!-- datepicker -->
        <script src="{{ URL::asset('admin/plugin/bootstrap-datepicker/bootstrap-datetimepicker.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::asset('admin/js/adminlte.min.js') }}"></script>
        <!-- DataTable -->
        <script src="{{ URL::asset('admin/plugin/datatable/datatables.min.js') }}"></script>
        <!-- CKEditor -->
        <script src="{{ URL::asset('admin/plugin/ckeditor/ckeditor.js') }}"></script>
        <!-- Selectpicker -->
        <script src="{{ URL::asset('admin/plugin/selectpicker/bootstrap-select.js') }}"></script>

        <script>
            // BOOTSTRAP TOOLTIPS
            if ($(window).width() > 767) {
                $(function () {
                    $('[rel="tooltip"]').tooltip()
                });
            };
        </script>

        @yield('javascript')
    </body>
</html>
