{{-- Modal Edit  --}}

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    @if ($error)
                        <span class="text-danger">{{ $error['fail'] }}</span>
                    @else
                        <h4 class="modal-title" id="myModalLabel">Edit Data</h4>
                    @endif
                </div>
                @if ($error)
                <div class="modal-body pad-20">
                    <p class="text-lgray text-right" style="position: absolute; top: 0; right: 0; margin: 10px;">
                            {{ $data->created_at->format('d-m-Y') }}
                            <br/>
                            <span class="small">{{ $data->created_at->format('H:i') }}</span>
                    </p>
                    <h2>
                        <p><b class="mb-5 text-green">{{$data->title}}</b></p>
                    </h2>
                    <h4>
                    <p class="mb-5 text-black">{{$data->name}}</p>
                    <p class="mb-5 text-black">{!! nl2br(e($data->body)) !!}</p>
                    </h4>
                        <div class="form-group row">
                            <div class="col-md-4">
                                @if($data->image)
                                    <img  style="height:150; float:left" class="img-responsive img-post" src="{{ $data->imageURL() }}" alt="image">
                                    @else 
                                    <img  style="height:150; float:left;" class="img-responsive img-post" src="{{asset('storage/images/default.jpg')}}" alt="image">
                                @endif
                            </div>
                        </div>
                  </div>

                    <div class="modal-footer">
                        <form action="{{ route('edit', $data->id) }}" method="POST" class="form-inline">
                            {{ csrf_field() }}
                            @if (strlen($error['fail']) !== 37)
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            @else
                                <div class="form-group mx-sm-3 mb-2 pull-left">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password2" autocomplete="new-password">
                                    <input type="hidden" name="id" value="{{ $data->id }}">
                                    <button type="submit" class="btn btn-default mb-2" name="edit" value="edit"><i class="fa fa-repeat p-3"></i></button>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
                @else
                    <form method="post" action="{{ route('update', $data->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $data->name) }}">
                                <p class="small text-danger mt-5">{{ $errors->edit->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title', $data->title) }}">
                                <p class="small text-danger mt-5">{{ $errors->edit->first('title') }}</p>
                            </div>
                            <div class="form-group">
                                <label>Body</label>
                                <textarea rows="5" name="body" class="form-control">{{ old('body', $data->body) }}</textarea>
                                <p class="small text-danger mt-5">{{ $errors->edit->first('body') }}</p>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    @if($data->image)
                                    <img  style="height:150; float:left" class="img-responsive img-post" src="{{ $data->imageURL() }}" alt="image">
                                    @else 
                                    <img  style="height:150; float:left;" class="img-responsive img-post" src="{{asset('storage/images/default.jpg')}}" alt="image">
                                    @endif
                                </div>
                                <div class="col-md-8 pl-0">
                                    <label>Choose image from your computer :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control upload-form" value="No file chosen" readonly autocomplete="nofilechosen">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="must" value="delete">Delete image
                                        </label>
                                    </div>
                                </div>
                                <p class="small text-danger mt-5">{{ $errors->edit->first('image') }}</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i style="font-size: 20" class="fa fa-close"></i></button>
                            <button type="submit" class="btn btn-primary"><i style="font-size: 20" class="fa fa-check"></i></button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
