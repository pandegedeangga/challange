{{-- Modal Delete  --}}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    @if ($error)
                        <span class="text-danger">{{ $error['fail'] }}</span>
                    @else
                        <h4 class="modal-title text-danger" id="myModalLabel"> Are You Sure Want to Delete This Data?</h4>
                    @endif
                </div>
                <div class="modal-body pad-20">
                    <p class="text-lgray text-right" style="position: absolute; top: 0; right: 0; margin: 10px;">
                            {{ $data->created_at->format('d-m-Y') }}
                            <br/>
                            <span class="small">{{ $data->created_at->format('H:i') }}</span>
                        </p>
                     <h2><p><b class="mb-5 text-green">{{$data->title}}</b></p></h2>
                     <h4>
                     <p class="mb-5 text-black">{{$data->name}}</p>
                     <p class="mb-5 text-black">{!! nl2br(e($data->body)) !!}</p>
                    </h4>
                        <div class="form-group row">
                            <div class="col-md-4">
                                @if($data->image)
                                    <img  style="height:150; float:left" class="img-responsive img-post" src="{{ $data->imageURL() }}" alt="image">
                                    @else 
                                    <img  style="height:150; float:left;" class="img-responsive img-post" src="{{asset('storage/images/default.jpg')}}" alt="image">
                                @endif
                            </div>
                        </div>
                  </div>


                <div class="modal-footer">
                    @if ($error)
                        <form action="{{ route('delete', $data->id) }}" method="POST" class="form-inline">
                            {{ csrf_field() }}
                            @if (strlen($error['fail']) !== 37)
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            @else
                                <div class="form-group mx-sm-3 mb-2 pull-left">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password2" autocomplete="new-password">
                                    <input type="hidden" name="id" value="{{ $data->id }}">
                                    <button type="submit" class="btn btn-default mb-2" name="delete" value="delete"><i class="fa fa-repeat p-3"></i></button>
                                </div>
                            @endif
                        </form>
                    @else
                        <form action="{{ route('destroy', $data->id) }}" method="POST" class="form-inline">
                            {{ csrf_field() }}
                            <input type="hidden" name="currentPage" value="{{ $posts->currentPage() }}">
                            <button type="submit" class="btn btn-danger">Yes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>



