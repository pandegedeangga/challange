@extends('users.layouts.app')
{{-- Submit Form --}}
@section('content')	

	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 bg-white p-30 box">
					<div class="text-center">
						<h1 class="text-green mb-30"><b>Level 6 Challenge</b></h1>
					</div>

					<form  method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
						<div class="form-group">
							<label>Name</label>
							<input type="text" class="form-control" name="name" value="{{ $errors->hasBag('index') ? old('name') : ''}}">
							<p class="small text-danger mt-5">{{ $errors->index->first('name') }}</p>
						</div>
						<div class="form-group">
							<label>Title</label>
							<input type="text" class="form-control" name="title" value="{{ $errors->hasBag('index') ? old('title') : ''}}">
							<p class="small text-danger mt-5">{{ $errors->index->first('title') }}</p>
						</div>
						<div class="form-group">
							<label>Body</label>
							<textarea rows="5" class="form-control" name="body">{{ $errors->hasBag('index') ? old('body') : ''}}</textarea>
							<p class="small text-danger mt-5">{{ $errors->index->first('body') }}</p>
						</div>
						<div class="form-group">
							<label>Choose image from your computer :</label>
							<p class="small text-danger mt-5">{{ $errors->index->first('image') }}</p>
								<div class="input-group">
									<input type="text" class="form-control upload-form" value="No file chosen" readonly>
									<span class="input-group-btn">
									<span class="btn btn-default btn-file">
										<i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
									</span>
									</span>
								</div>
						</div>
						@guest
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control">
							<p class="small text-danger mt-5">{{ $errors->index->first('password') }}</p>
						</div>
						@endguest
						<div class="text-center mt-30 mb-30">
							<button formaction="{{ route('submit') }}" class="btn btn-primary">Submit</>
						</div>
					</form>
				
					{{-- Session Success --}}
					@if(session('success'))
						<div class="alert alert-success" role="alert">
							{{session('success')}}
							<button type="button" class="close" data-dismiss="alert">×</button>
						</div>
					@endif

					@foreach ($posts as $post)
					<div class="post">
						<div class="clearfix">
							<div class="pull-left">
								<h2  class="mb-5 text-green"><b>{{$post->title}}</b></h2>
							</div>
							<div class="pull-right text-right">
								<p class="text-lgray"><p class="text-lgray">
								{{ $post->created_at->format('d-m-Y') }}
								<br/>
								<span class="small">
									{{ $post->created_at->format('H:i') }}
								</span>
								</p>
							</div>
						</div>
						<h4 class="mb-20">
							{{$post->name ?? "No Name"}}
							<span class="text-id">{{ $post->user_id ? "Member [ID: {$post->user_id}]" : "No Member" }}</span>
						</h4>
						<p>{!! nl2br(e($post->body)) !!}</p>
						<div class="img-box my-10">
							@if($post->image)
							<img  style="height:320" class="img-responsive img-post" src="{{ $post->imageURL() }}" alt="image">
							@else 
							<img  style="height:320" class="img-responsive img-post" src="{{ asset('storage/images/default.jpg') }}" alt="image">
							@endif
						</div>
							
					{{-- Form Edit dan Delete Modal --}}
					<form method="POST" class="form-inline mt-50">
						{{csrf_field()}}
						@if(Auth::user() && Auth::id() === $post->user_id)
								{{-- <div class="form-group mx-sm-3 mb-2">
									<label for="password2" class="sr-only">Password</label>
									<input type="password" class="form-control" name="password2" placeholder="Password">
								</div> --}}
								<button type="submit" formaction="{{ route('edit', $post->id) }}" value="edit" name="edit" class="btn btn-default mb-2"  data-toggle="modal">
									<i class="fa fa-pencil p-3"></i>
								</button>
								<button type="submit" formaction="{{ route('delete', $post->id) }}" value="delete" name="delete" class="btn btn-danger mb-2" data-toggle="modal">
									<i class="fa fa-trash p-3"></i>
								</button>
							@elseif(Auth::guest() && empty($post->user_id))
								<div class="form-group mx-sm-3 mb-2">
									<label for="password2" class="sr-only">Password</label>
									<input type="password" class="form-control" name="password2" placeholder="Password" autocomplete="rutjfkde">
								</div>
								<button type="submit" formaction="{{ route('edit', $post->id) }}" value="edit" name="edit" class="btn btn-default mb-2"  data-toggle="modal">
									<i class="fa fa-pencil p-3"></i>
								</button>
								<button type="submit" formaction="{{ route('delete', $post->id) }}" value="delete" name="delete" class="btn btn-danger mb-2" data-toggle="modal">
									<i class="fa fa-trash p-3"></i>
								</button>
							@endif
					</form>
					</div>	
					@endforeach	
				
					<div  class="text-center mt-30">
						<nav>
							<ul class="pagination">
								<li>{{ $posts->links() }}</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection