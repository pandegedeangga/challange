@extends('users.layouts.auth')
{{-- @extends('layouts.app') --}}

@section('content')
<div class="box login-box text-center">
    <div class="login-box-head">
    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            {{ __('A fresh verification link has been sent to your email address.') }}
        </div>
    @elseif (session('invalidEmail'))
        <div class="alert alert-danger" role="alert">
            {{ __('This email has not been registered yet.') }}
        </div>
    @endif

    <h4>
        {{ __('Resend Verification E-Mail.') }}
    </h4>
    
    <div class="card-body">
        <form method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <div class="login-box-body">
                <div class="form-group mt-20">
                    <label>Email :</label>
                    <input id="email" type="email" class="form-control" name="email" placeholder="E-mail">
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Send</button>
                    <a href="/"  class="btn btn-primary">Home</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
