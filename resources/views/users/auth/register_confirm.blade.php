@extends('users.layouts.auth')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            <h1>Register</h1>
        </div>
        <div class="login-box-body">
            <table class="table table-no-border">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <td>{{ $request->name }}</td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $request->email }}</td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td>{{ $request->password }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="login-box-footer">
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}
                    <input type="hidden" name="name" value="{{ $request->name }}">
                    <input type="hidden" name="email" value="{{ $request->email }}">
                    <input type="hidden" name="password" value="{{ $request->password }}">
                    <div class="text-right">
                        <button formaction="{{ route('register') }}" type="submit" class="btn btn-primary">Submit</button>
                    </div>
            </form>
        </div>
    </div>

@endsection