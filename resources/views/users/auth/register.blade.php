@extends('users.layouts.auth')

@section('content')
<div class="box login-box">
    <div class="login-box-head">
        <h1 class="mb-5">Register</h1>
        <p class="text-lgray">Please fill the information below...</p>
    </div>

    <form method="POST" action="{{ route('confirm') }}">
        {{csrf_field()}}
        <div class="login-box-body">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Name"  name="name" value="{{ $data['name'] ?? old('name') }}">
                <p class="small text-danger mt-5">{{ $errors->first('name') }}</p>
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="E-mail" name="email" value="{{ $data['email'] ?? old('email') }}">
                <p class="small text-danger mt-5">{{ $errors->first('email') }}</p>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" value="{{ $data['password'] ?? old('password') }}">
                <p class="small text-danger mt-5">{{ $errors->first('password') }}</p>
            </div>
        </div>
        <div class="login-box-footer">
            <div class="text-right">
                <a href="{{route ('post') }}" class="btn btn-default">Back</a>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </div>
        </div>
    </form>
</div>
@endsection


{{-- @extends('users.layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
