@extends('users.layouts.auth')

@section('content')
    <div class="box login-box text-center">
        <div class="login-box-head">
            <h1>Member Not Verified</h1>
        </div>
        <div class="login-box-body">
            {{ __('Before login, please check your email for a verification link.') }}
            {{ __('If you did not receive the email') }}, You can resend the verification email.
        </div>
        <div class="login-box-footer">
            <div class="text-center">
                <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
                <a href="{{ route('showResend') }}" type="submit" class="btn btn-primary">Resend</a>
            </div>
        </div>
    </div>
    
@endsection
