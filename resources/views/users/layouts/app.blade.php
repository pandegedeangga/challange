<html>
  <head>
      <title>Timedoor Challenge - Level 6</title>
      <!-- CSS -->
      <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('css/tmdrPreset.css')}}">
      <!-- CSS End -->

      <!-- Javascript -->
      <script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
      <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
      <!-- Javascript End -->
 
      {{-- Session Data --}}
      @php
      $data = Session::get('post')
      
      @endphp

      @php
      $error = Session::get('eror')
      @endphp

      @if (Session::get('deleteModal'))
      <script>
          $(function() {
            $('#deleteModal').modal('show');
          });
      </script>

      @elseif (Session::get('editModal'))
      <script>
          $(function() {
            $('#editModal').modal('show');
          });
      </script>
      @endif

      <script>
        // INPUT TYPE FILE
        $(document).on('change', '.btn-file :file', function() {
          var input = $(this),
              numFiles = input.get(0).files ? input.get(0).files.length : 1,
              label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
          input.trigger('fileselect', [numFiles, label]);
        });
      
        $(document).ready( function() {
          $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;
      
            if( input.length ) {
              input.val(log);
            } else {
              if( log ) alert(log);
            }
          });
        });
      </script>
</head>

<body class="bg-lgray">
  <header>
    <nav class="navbar navbar-default" role="navigation">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <h2 class="font16 text-green mt-15"><b>Timedoor 30 Challenge Programmer</b></h2>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            @guest
            <li><a href="{{ route('login') }}">Login</a></li>
            <li><a href="{{ route('register') }}">Register</a></li>
             @else
            <li><a href="{{ route('logout') }}">Logout</a></li>
            @endguest
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
  </header>

<main>
    @yield('content')
</main>


<footer>
    <p class="font12">Copyright &copy; <script>document.write(new Date().getFullYear());</script> by <a href="https://timedoor.net" class="text-green">PT. TIMEDOOR INDONESIA</a> </p>
</footer>
  
@if($data)
  @include('users.post.modal.modalDelete');
  @include('users.post.modal.modalEdit');
@endif
 
  </body>
</html>