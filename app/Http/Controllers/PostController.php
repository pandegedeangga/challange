<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Http\Requests\UpdateDataRequest;
use App\Http\Requests\SumbitDataRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


Use Image;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        return view("users.post.index",['posts' => $posts]);
    }

    public function submit(SumbitDataRequest $request)
    {
        
        
        $post           = new Post;
        $post->name     = $request->name;
        $post->title    = $request->title;
        $post->body     = $request->body;
       

        if ($originalImage = $request->file('image')) {
            $post['image'] = $this->saveImage($originalImage);
        }
        
        if (!empty($request->password)) {
            $post->password=hash::make($request->password);
        }

        if (Auth::user()) {
            $post->user_id = Auth::id();
            $post->password = hash::make($request->password);
        } 
        
        $post->save();
        
        return redirect()->back()->with('success', "Successfuly Added.");
    }

    public function edit (Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->checkPassword($post->password, $request->password2, $request->edit);

        $data = 
        [ 
            "{$request->edit}Modal"      =>  "{$request->edit}Modal", 
            'post'                       =>  $post, 
            isset($error) ? 'eror' : 'n' =>  isset($error) ? $error : null
        ];
        
        return redirect()->back()->with($data);
        
    }

   public function update (UpdateDataRequest $request)
   {

    if ($request->validated()) {
        $post = Post::findOrFail($request->id);
        $this->checkPassword($post->password, $request->password2);
        $data = request()->except(['password']);
    }

    if ($request->must) {
        $post->deleteImage($post->image);
        $data['image'] = NULL;
    } else if ($request->image) {
      $post->deleteImage($post->image);
      $data['image'] = $this->saveImage($request->image);   
    }
    $post->update($data);
    
    return redirect()->back()->with('success', "Successfuly Updated.");
    
    }

    public function delete (Request $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->checkPassword($post->password, $request->password2, $request->delete);

        $data = 
        [ 
            "{$request->delete}Modal"    =>  "{$request->delete}Modal", 
            'post'                       =>   $post, 
            isset($error) ? 'eror' : 'n' => isset($error) ? $error : null
        ];

        return redirect()->back()->with($data);
    }

    public function destroy(Request $request)
    {
        $post = Post::findOrFail($request->id);

        $this->checkPassword($post->password, $request->password2);

        $post->deleteImage($post->image);
        $post->delete();

        $result = Post::paginate(5);
        $lastPage = $result->lastPage();
       
        if ($request->currentPage > $lastPage) { 
            $url = '/post?page=' . $lastPage;
            return redirect($url)->with('success', 'Deleted');
        } else {
            return redirect()->back()->with('success', 'Deleted');
        }
    }

    public function saveImage($originalImage)
    {
        $name = time() . $originalImage->getClientOriginalName();
        $dataImage = Image::make($originalImage);

        if ( ! \File::isDirectory($originalPath = public_path() . Post::$originalPath) ) {
            \File::makeDirectory($originalPath, 493, true);
        }

        if ( ! \File::isDirectory($thumbnailPath = public_path() . Post::$thumbnailPath) ) {
            \File::makeDirectory($thumbnailPath, 493, true);
        }

        $dataImage->save($originalPath . $name);
        $dataImage->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });
        $dataImage->save($thumbnailPath . $name);

        return $name;
    }

    public function checkPassword($dbPassword, $inputPassword, $modalName="")
    {

        // Query Lama
        if (!$dbPassword) {
            $error = ["fail" => "This post can't {$modalName}, because not been set password."];
        } else if (!Hash::check($inputPassword, $dbPassword)) {
            $error = ["fail" => "Password Not Match. Please try again."];
        }
        
        $data = 
        [ 
            null, 
            null, 
            isset($error) ? 'eror' : 'n' => isset($error) ? $error : null
        ];
        return redirect()->back()->with($data);
    }
}