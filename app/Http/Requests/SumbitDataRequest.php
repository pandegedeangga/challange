<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


class SumbitDataRequest extends FormRequest
{

    protected $errorBag = 'index';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'      => 'required|between:3,16',
            'title'     => 'required|between:10,32',
            'body'      => 'required|between:10,200',
            'password'  => 'nullable|numeric|digits_between:4,10',
            'image'     => 'image|mimes:jpeg,jpg,png,gif|max:1024' 
        ];

        return $rules;
    }
}
