<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


use App\Models\Post;
class UpdateDataRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'      => 'required|between:3,16',
            'title'     => 'required|between:10,32',
            'body'      => 'required|between:10,200',
            'password'  => 'nullable|numeric|min:4',
            'image'     => 'image|mimes:jpeg,jpg,png,gif|max:2048'      
        ];
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        $post = Post::find($this->id);

        return redirect()->back()
            ->withErrors($validator, 'edit')
            ->with([
                'editModal'     => 'editModal',
                'post'          =>  $post
            ]);
    }
}

