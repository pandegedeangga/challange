<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{

    const IMAGE_PATH = '/storage/images/posts';

    protected $fillable = [ 'name', 'title', 'body', 'image', 'password' ];


    public static $thumbnailPath = self::IMAGE_PATH . '/thumbnail/';
    public static $originalPath = self::IMAGE_PATH . '/original/';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function imageURL($type = 'original')
    {
        if ($this->image) {
            return self::IMAGE_PATH . '/' . $type . '/' . $this->image;
        } 
    }

    public function deleteImage()
    {
        if ($this->image) {
            // unlink image in original and also thumbnail path
            if (file_exists($thumbnailImage = public_path() . Post::$thumbnailPath . $this->image)) {
                unlink($thumbnailImage);
            }
            if (file_exists($originalImage = public_path() . Post::$originalPath . $this->image)) {
                unlink($originalImage);
            }
        }
    }
}
