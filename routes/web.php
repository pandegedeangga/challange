<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('post');
});

Route::get('/post', 'PostController@index')->name('post');
Route::post('/post/submit', 'PostController@submit')->name('submit');
Route::post('/post/edit/{id}', 'PostController@edit')->name('edit');
Route::post('/post/update/{id}', 'PostController@update')->name('update');
Route::post('/post/destroy/{id}', 'PostController@destroy')->name('destroy');
Route::post('/post/delete/{id}', 'PostController@delete')->name('delete');

Auth::routes(['verify' => true]);

Route::get('/logout', function() {
    Auth::logout();
    return redirect('/');
});

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::post('/confirm', 'Auth\RegisterController@confirm')->name('confirm');
Route::get('/resend', 'Auth\VerificationController@showResend')->name('showResend');